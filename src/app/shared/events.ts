export enum WSEvents {
    connection = "connection",
    disconnect = "disconnect",
    message = "message",
    tictactoe = "tictactoe",
    startGame = "startGame",
    tictactoeRedirection = "tictactoeRedirection",
    move = "move"
}