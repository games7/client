import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicTacToeComponent } from './tic-tac-toe.component';
import { BoardComponent } from './board/board.component';
import { SquareComponent } from './square/square.component';



@NgModule({
  declarations: [
    TicTacToeComponent,
    BoardComponent,
    SquareComponent
  ],
  imports: [
    CommonModule
  ]
})
export class TicTacToeModule { }
