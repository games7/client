import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styles: ["button { height: 100px; width: 100px; }"]
})
export class SquareComponent implements OnInit {
  @Input() row: number;
  @Input() col: number;
  @Input() value: string = "";
  @Input() disabled: boolean;
 
  constructor() { }

  ngOnInit(): void {
  }

  handleSquareClick() {
    console.log("Square click", this.row, this.col);
  }
   

}
