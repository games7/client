import { Component, OnInit } from '@angular/core';
import { WebsocketService } from '../services/websocket.service';
import { WSEvents } from '../shared/events';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent {
  Games = [
    "Tic Tac Toe",
    "Chess",
    "Otello"
  ]
  
  constructor(private wsService: WebsocketService) {
    //TODO: service to get available games
  }

  joinQueueGame(game, playerName) {
    const data = {game, playerName};
    this.wsService.emit(WSEvents.startGame, data);
  }

}
