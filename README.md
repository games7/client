# Games

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.
There must be a server listening to handle sockets request.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build
Remember to set the right IP address of the Node server, where it will be hosted in the environment.prod.ts file
Bundle files must be copied to Node server project (dist folder), but the app can be run from dev server in port 4200.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
